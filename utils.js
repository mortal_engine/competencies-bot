import 'dotenv/config';
import fetch from 'node-fetch';
import { verifyKey } from 'discord-interactions';
import * as cheerio from 'cheerio';
import axios from 'axios';

export async function GetValidatedComps(spe, proj, cookie) {
  const gandalfCookie = cookie;
  const gandalfCompPage = await axios.get(
    'https://gandalf.epitech.eu/local/graph/view.php',
    {
      headers: { Cookie: `MoodleSession=${gandalfCookie};` }
    }
  );

  const $ = cheerio.load(gandalfCompPage.data);
  const validatedComps = [];
  $('div.behaviorLine').each((i, div) => {
    const comp = $(div).children('.competencyTitle').text().trim();
    const status = $(div).children('.proficiencyIcon').attr('title').trim(); // unrated, failed, success

    if (status === 'success') {
      validatedComps.push({ 'behaviorCode': comp.split('-')[0].trim(), 'behaviorText': comp.split('-')[1].trim() });
    }
  });

  return validatedComps;
}

export function VerifyDiscordRequest(clientKey) {
  return function (req, res, buf, encoding) {
    const signature = req.get('X-Signature-Ed25519');
    const timestamp = req.get('X-Signature-Timestamp');

    const isValidRequest = verifyKey(buf, signature, timestamp, clientKey);
    if (!isValidRequest) {
      res.status(401).send('Bad request signature');
      throw new Error('Bad request signature');
    }
  };
}

export async function DiscordRequest(endpoint, options) {
  const url = 'https://discord.com/api/v9/' + endpoint;
  if (options.body) options.body = JSON.stringify(options.body);

  const res = await fetch(url, {
    headers: {
      Authorization: `Bot ${process.env.DISCORD_TOKEN}`,
      'Content-Type': 'application/json; charset=UTF-8',
    },
    ...options
  });

  if (!res.ok) {
    const data = await res.json();
    console.log(res.status);
    throw new Error(JSON.stringify(data));
  }

  return res;
}