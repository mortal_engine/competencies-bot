FROM node:lts-hydrogen

WORKDIR /glandalf-bot

COPY ./package*.json ./

RUN npm ci

COPY . .

CMD ["node", "app.js"]
